package com.jt.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.jt.mapper.EmployeeMapper;
import com.jt.pojo.Employee;
import com.jt.service.EmployeeService;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee>
        implements EmployeeService {

}
