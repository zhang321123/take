package com.jt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jt.pojo.Employee;


public interface EmployeeService extends IService<Employee> {
}
